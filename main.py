#!/usr/bin/python3.5
import urllib3
import re
import time
import threading
from database import DataBase# imports the database class (simplified [even more simplified than python itself {is it possible? (i thing so)}])

db = DataBase()

url_array = []
content_array = []
http = urllib3.PoolManager()

#header ... Location: "http://www.mksolutions.com.br/novosite/"

user_agent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:46.0) Gecko/20100101 Firefox/46.0"
#start_url = "http://www.mksolutions.com.br/novosite/"
start_url = "http://mksolutions.com.br"
start_url_list = [start_url]
headers = {}
headers['User-Agent'] = user_agent # aqui precisa adicionar a URL, pois se tiver virtualhost no apache não funciona, a biblioteca abre socket via IP (obviamente)
###################################################################################################
class getContentThread(threading.Thread):
    def __init__(self, tid, url):
        self.tid = tid
        self.url = url
        self.content = ""
        self.done = False
        threading.Thread.__init__(self)
    def run(self):
        self.content = getContent(self.url)
        self.done = True
###################################################################################################
def getContent(url):
    try:
        request = http.request('GET', url)
        request.headers['User-Agent'] = user_agent
        url_content = ""
        url_content+=str(request.data)
        request.close() # isso fecha?
    except Exception as erro:
        url_content = erro
    return url_content
###################################################################################################
def checkUrlIsFile(url):
    url_is_file=False
    not_indexable_list = db.query("SELECT extension FROM file_types WHERE category = 1;")
    for x in not_indexable_list:
        if "."+str(x[0]) in url:
            url_is_file=True
    return url_is_file
###################################################################################################
def checkUrlIsAbsolute(url):
    url_is_absolute = False
    if url[0:4] == "http":
        url_is_absolute = True
    return url_is_absolute
###################################################################################################
def checkUrlIsRelative(url):
    url_is_relative = False
    if url[0] != "#" and (url[0:4] != "http" or url[0] == "/" or "." in url == False):
        url_is_relative = True
    return url_is_relative
###################################################################################################
def checkUrlIsInternatLink(url):
    url_is_internal_link = False
    if url[0] == "#":
        url_is_internal_link = True
    return url_is_internal_link
###################################################################################################
def getUrlsFromContent(content = ""):
    content = content.replace('"', "'")
    sub = content.split("'")###other shity shit
    temp_url_array = []
    print("-"*150)
    for x,y in enumerate(sub):
        if sub[x-1][-5:] == "href=":
            y = (y.strip())
            if y not in url_array and y not in temp_url_array:
                if len(y) > 0:
                    if checkUrlIsRelative(y) == True:
                        print("Relative Link: ("+y+")")#todo -> remount URL
                    elif checkUrlIsInternatLink(y) == True:
                        print("Internal link: ("+y+")")#do nothing
                    elif checkUrlIsFile(y) == True:
                        print("File: ("+y+")")# todo -> donload file depending on the extension
                    elif checkUrlIsAbsolute(y) == True:
                        temp_url_array.append(y)
                        print("Absolute URL: ("+y+")")#do nothing
                    else:
                        print("What is this? ("+y+")")
                #elif y[0] == "#":# é redirecionamento dentro do próprio html...
                    #fazer adicionar na lista de arquivos
                #se começar com //, precisa compor com o link absoluto (será a url pai dele + ele...)
                #fazer verificação de extensões de arquivos, e botar um módulo separado a baixar eles
                # fazer também ele recompor o link (tirando uma pasta, tirando outra pasta...) - portanto podem ser ignorados os ".."
    return temp_url_array
###################################################################################################
def recursiveLinks(url_list):
    thread_list = []

    for x,y in enumerate(url_list):
        if str(y) not in url_array:
            thread_list.append(getContentThread(x, y))
            thread_list[x].start()
    while True:
        time.sleep(0.3)
        done = True
        for x in thread_list:
            if x.done == False:
                done = False
        if done == True:
            break
    for x in thread_list:
        if x.url not in url_array:
            url_array.append(x.url)
            content_array.append(str(x.content))
            new_url_list = getUrlsFromContent(str(x.content))
            recursiveLinks(new_url_list)
###################################################################################################
recursiveLinks(start_url_list)
print(url_array)
#print(content_array)
db.close()
